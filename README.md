css note
===
---
## display8888
* position:absolutt 
    * width:100%
        * `(width=parent width)`   
    * width:none
        * `(width=chilid content)`
 * if parent width smaller than child and they with background-color bothly, you will see parent cover by child
    * child seeting positon ab z-index -1 
    * parent setting positioin relative can resolve
* box-shadow
    * property 
        * spread 
            * base  on  element border 
            * same level with margin
* position ab element location of parent border
* if you want text-overflow:ellipsis  setting below
    ```
    text-overflow:ellipsis;
    overflow:hidden 
    white-space:no-warp
    width:npx
    display:inline-block
    ```
* table 
    * width 
        * (child <parent) base on parent 
        * (child >parent) base on content
    * every column width are equal
    * Margin wont work with table cells,Try padding or cellspacing="" to table. Or add a div inside the table cell and apply margin to the div.
    * if you want to padding td item you can do like this below
         ```
          border-collapse: separate;
          border-spacing: 0.5rem 0;
         ```
    * if you want border-bottom with tr you need to do like below
       ```
       table {
         border-collapse: collapse;
             }
       ```
* flex
   * child max--width equal parent width(cant excess parent width)
   * if child is block element and wrapped by inline element that width will  inherrit most  close block element  
*  div
  *無預設寬度會依據內容當作寬度  
* 背影圖片會蓋掉背景顏色  
* **background顏色會涵蓋到padding的區域**
* 寬度繼承預設以width為繼承目標,不受padding,border影響
