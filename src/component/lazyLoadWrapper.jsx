import React from "react";
import styled from "styled-components";
import axios from "axios";
import LazyLoad from "react-lazyload";

class LazyLoadWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
  }

  render() {
    const { children } = this.props;
    return (
      <MyLazyLoad
        placeholder={<Loading />}
        once={true}
        height={100}
        offset={0}
        resize={true}
      >
        {children}
      </MyLazyLoad>
    );
  }
}

const MyLazyLoad = styled(LazyLoad)``;

const MyImage = styled.img`
  width: 300px;
  height: 300px;
  margin-bottom: 10px;
`;

const Loading = styled.div`
  min-height: 300px;
  background-color: red;
  margin-bottom: 10px;
`;

export default LazyLoadWrapper;
