/* eslint-disable linebreak-style */
import React from 'react';
import styled from 'styled-components';
import { Circle } from '../withCircle';
import { WithBorder } from '../withBorder';

const StyleWrapper = styled.div`
  /* .big-card__content {
    position: relative;
    .content-footer {
      position: absolute;
      bottom: 0%;
    }
    .card__fav-icon {
      position: absolute;
      right: 0;
    }
  } */
  .basicAlert {
    position: fixed;
    right: 20px;
    bottom: 20px;
    width: 350px;
    z-index: 5;
    .basic-alert-btn {
    }
  }
`;

function BasicAlert() {
  return (
    <StyleWrapper>
      <div className="basicAlert">
        <WithBorder
          bgColor="light"
          boxShadow="0 0.55rem 1.5rem rgba(34,34,34,0)"
          padding="15px"
        >
          <div className="register-slide-in d-flex justify-between">
            <span className="basic-alert-title text-lv6">還未註冊</span>
            <Circle circleSize="base" borderColor="gray">
              <div className="fas fa-times-circle"></div>
            </Circle>
          </div>
          <p>
            當您註冊您的Catawiki免費帳戶，您將能夠在我們每週達50000件特別物品中的拍賣上競投。
          </p>
          <div className="basic-alert-btn col-md-6">
            <WithBorder
              bgColor="warning"
              borderRadius="26"
              padding="10px 0"
              textAlign="center"
            >
              <a href="">
                <span>建立新帳戶</span>
              </a>
            </WithBorder>
          </div>
        </WithBorder>
      </div>
    </StyleWrapper>
  );
}
export default BasicAlert;
