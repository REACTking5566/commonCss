/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import InputSearch from "../component/inputSearch";
import { WithBorder } from "../component/withBorder";
import { ToggleItem } from "../component/toggle/toggleItem";
import BasicBtn from "../component/button/basicBtn";
import TranslateBtn from "../component/i18n/TranslateBtn";
import { Collapse } from "reactstrap";

const StyleWrapper = styled.div`
  .main-banner__img {
    width: 100%;
    height: auto;
  }
  .main-banner {
    min-height: 5rem;
    width: 100%;
    .main-banner__inner {
      position: relative;
      height: 100%;
      text-align: center;
      span {
        color: ${({ theme: { color } }) => color["light"]};
      }
    }
    .main-banner__describle {
      position: absolute;
    }
    .main-banner__bg {
      background-image: ${({ mainBannerBgUrl }) => `url(${mainBannerBgUrl})`};
      height: 300px;
    }
  }
`;
const MainBanner = ({ mainBannerBgUrl }) => {
  const [showMobileHeader, setMobileHeader] = useState(false);
  return (
    <StyleWrapper mainBannerBgUrl={mainBannerBgUrl}>
      <div className="main-banner">
        <div className="main-banner__bg">
          <div className="main-banner__inner">
            <div className="main-banner__describle abs-both">
              <div className="main-banner__title m-b-15">
                <span className="text-lv3">Unique Objects</span>
              </div>
              <div className="main-banner__subTitle m-b-15">
                <span className="text-lv6">Selected by our experts</span>
              </div>
              <div className="main-banner__discover">
                <WithBorder
                  bgColor="warning"
                  borderRadius="26"
                  padding="10px 0"
                >
                  <a href="">
                    <span className="text-lv6">discover more</span>
                  </a>
                </WithBorder>
              </div>
            </div>
          </div>
        </div>
      </div>
    </StyleWrapper>
  );
};

export default MainBanner;
