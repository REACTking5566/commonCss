/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

const BorderWrapper = styled.div`
  border-radius: ${({ borderRadius }) => borderRadius || 0}px;
  background-color: ${({ theme: { color }, bgColor }) => color[bgColor]};
  overflow: ${({ overflowHideen }) => overflowHideen && 'hidden'};
  padding: ${({ padding }) => padding || 0};
  border: ${({ border, theme: { color } }) =>
    border || `1px solid ${color['secondary']}`};
  position: ${({ relative }) => relative};
  text-align: ${({ textAlign }) => textAlign};
  box-shadow: ${({ boxShadow }) => boxShadow};
`;

export const WithBorder = ({
  bgColor,
  overflowHideen,
  borderRadius,
  children,
  padding,
  border,
  relative,
  textAlign,
  boxShadow,
}) => (
  <BorderWrapper
    overflowHideen={overflowHideen}
    bgColor={bgColor}
    borderRadius={borderRadius}
    padding={padding}
    border={border}
    relative={relative}
    textAlign={textAlign}
    boxShadow={boxShadow}
  >
    {children}
  </BorderWrapper>
);
