/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Circle } from "../withCircle";
import { WithBorder } from "../withBorder";
import BasicInput from "../input/basicInput";
const StyleWrapper = styled.div`
  .filter__checkbox {
    min-width: 1rem;
  }
`;

function BasicFilter({ text }) {
  return (
    <StyleWrapper>
      <label htmlFor="" className="filter__label d-flex align-item-center">
        <div className="filter__checkbox m-r-15">
          <BasicInput inputType="checkbox" inputSize="sm" />
        </div>
        <span className="filter__text">
          <h3 className="text-lv6">{text}</h3>
        </span>
      </label>
    </StyleWrapper>
  );
}
export default BasicFilter;
