import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import classnames from "classnames";

const TabItem = ({ active, onClick, children }) => {
  const tabStyle = {
    "maxWidth": "150px",
    color: active ? "red" : "green",
    border: active ? "1px red solid" : "0px",
  };
  return (
    <div style={tabStyle} onClick={onClick}>
      {children}
    </div>
  );
};

export default TabItem;
