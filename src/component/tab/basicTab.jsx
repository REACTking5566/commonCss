import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Collapse, Button, CardBody, Card } from "reactstrap";
import classnames from "classnames";

const BasicTab = ({ children, toggleTitle, row }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const classTag = classnames({ "justify-between": true }, { "d-flex": row });
  const newChildren = React.Children.map(children, (child, index) => {
    if (child.type) {
      return React.cloneElement(child, {
        active: activeIndex === index,
        onClick: () => setActiveIndex(index),
      });
    } else {
      return child;
    }
  });
  return <div className={`col-xs-4 ${classTag}`}>{newChildren}</div>;
};

export default BasicTab;
