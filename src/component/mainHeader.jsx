/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import InputSearch from "../component/inputSearch";
import { WithBorder } from "../component/withBorder";
import ToggleItem from "../component/toggle/toggleItem";
import BasicBtn from "../component/button/basicBtn";
import TranslateBtn from "../component/i18n/TranslateBtn";
import { Collapse } from "reactstrap";

const StyleWrapper = styled.div`
  @media (max-width: 768px) {
    .main-header__search,
    .main-header__help,
    .main-header__sell,
    .main-header__sign {
      display: none;
    }
  }
  @media (min-width: 768px) {
    .main-header__search-m-logo {
      display: none;
    }
    .main-header__sign-m {
      display: none;
    }
  }
  .main-header__classic-logo {
    min-height: 3rem;
    align-items: center;
    background-color: ${({ theme: { color } }) => color["info"]};
    color: #fff;
    padding: 0 1rem;
  }
  .main-header__help {
    span {
      color: ${({ theme: { color } }) => color["dark"]};
    } 
  }
`;

const MainHeader = ({
  bgColor,
  overflowHideen,
  borderRadius,
  children,
  padding,
  border,
  relative,
  textAlign,
  color,
}) => {
  const [showMobileHeader, setMobileHeader] = useState(false);
  return (
    <StyleWrapper>
      <div className="main-header">
        <div className="main-header__inner">
          <div className="d-flex flex-wrap align-item-center justify-between">
            <div className="main-header__classic-logo d-flex">
              <h3 className="text-lv7">競標網站</h3>
            </div>
            <div className="main-header__categories">
              <ToggleItem toggleTitle="cartegories" />
            </div>
            <div className="main-header__search col-md-5 col-xs-5">
              <WithBorder
                padding="3px"
                borderRadius={26}
                border={`1px solid ${color["info"]}`}
              >
                <InputSearch />
              </WithBorder>
            </div>
            <div
              className="main-header__search-m-logo"
              onClick={() => setMobileHeader(!showMobileHeader)}
            >
              <div className="fas fa-search"></div>
            </div>
            <div className="main-header__help">
              <a href="">
                <span className="text-lv7">help</span>
             </a>
            </div>
            <TranslateBtn />
            <div className="main-header__favorite">
              <div className="fas fa-heart"></div>
            </div>
            <div className="main-header__sign">
              <BasicBtn>SIGN IN</BasicBtn>
            </div>
            <div className="main-header__sign-m">
              <div className="fas fa-user-alt"></div>
            </div>
          </div>
          <Collapse isOpen={showMobileHeader}>
            <div className="main-header__search-m col-xs-12">
              <WithBorder
                padding="3px"
                borderRadius={26}
                border={`1px solid ${color["info"]}`}
              >
                <InputSearch />
              </WithBorder>
            </div>
          </Collapse>
        </div>
      </div>
    </StyleWrapper>
  );
};

export default MainHeader;
