/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { WithBorder } from "../withBorder";

const StyleWrapper = styled.div``;

function BasicBtn({ children }) {
  return (
    <StyleWrapper>
      <WithBorder
        padding="5px 1rem"
        borderRadius={26}
        bgColor="info"
        border="1px solid gray"
        textAlign="center"
      >
        {children}
      </WithBorder>
    </StyleWrapper>
  );
}
export default BasicBtn;
