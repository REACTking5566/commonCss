/* eslint-disable linebreak-style */
import React, {
  Component,
  useEffect,
  useState,
  Fragment,
  useRef,
  Children,
} from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Circle } from "../component/withCircle";
import Slider from "react-slick";

const settings = {
  dots: false,
  infinite: true,
  speed: 1000,
  slidesToShow: 5,
  slidesToScroll: 5,
  // autoplay: false,
  // speed: 3000,
  // autoplaySpeed: 2000,
  // cssEase: "linear",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const StyleWrapper = styled.div`
  max-width: ${({ maxWidth }) => maxWidth};
  position: relative;
  margin: 0 auto;
  .leftIcon {
    position: absolute;
    left: -1.5rem;
    &:focus {
      border: 0px;
    }
  }
  .rightIcon {
    position: absolute;
    right: -1.5rem;
  }
  /* margin: 0 auto; */
`;
class SlickWrapper extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const { children, maxWidth } = this.props;
    return (
      <StyleWrapper maxWidth={maxWidth}>
        <Slider {...settings} ref={(c) => (this.slider = c)}>
          {children}
        </Slider>
        <div style={{ textAlign: "center" }}>
          <div className="leftIcon abs-y-center" onClick={this.next}>
            <Circle circleSize="md">
              <div className="fas fa-arrow-left" />
            </Circle>
          </div>
          <div className="rightIcon abs-y-center" onClick={this.previous}>
            <Circle circleSize="md">
              <div className="fas fa-arrow-right" />
            </Circle>
          </div>
        </div>
      </StyleWrapper>
    );
  }
}
export default SlickWrapper;
