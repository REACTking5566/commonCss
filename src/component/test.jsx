/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Triangle } from "./withTags";

const StyleWrapper = styled.div`
  .gay {
    height: 100px;
    width: 200px;
    background: blue;
    position: relative;
  }
  .footer {
    /* height: 100px;
    width: 200px; */
    position: absolute;
    z-index: -2;
    border-radius: 6px;
    background-color: red;
    border: 1px solid black;
    &:hover {
      box-shadow: 0 0 12px 1.5px gray;
    }
    /* display: inline-block; */
  }
  .bg {
    height: 20px;
    /* border: 1px solid green; */
    padding: 200px;
  }
`;
function Footer({ text }) {
  return (
    <Triangle left={-15}>
      <div className="card__icon" />
      <StyleWrapper>
        <div className="gay">
          <section className="footer">
            <div className="bg">{text}</div>
          </section>
        </div>
      </StyleWrapper>
    </Triangle>
  );
}
export default Footer;
