import React from "react";
import { SRLWrapper } from "simple-react-lightbox";

const options = {
  overlayColor: "rgb(25, 136, 124)",
  captionColor: "#a6cfa5",
  captionFontFamily: "Raleway, sans-serif",
  captionFontSize: "22px",
  captionFontWeight: "300",
  captionFontStyle: "capitalize",
  buttonsBackgroundColor: "red",
  buttonsIconColor: "rgba(126, 172, 139, 0.8)",
  autoplaySpeed: 1500,
  transitionSpeed: 900,
  showDownloadButton: true,
};
const LightBoxWrapper = ({ children }) => (<SRLWrapper options={options}>{children}</SRLWrapper>);

export default LightBoxWrapper;
