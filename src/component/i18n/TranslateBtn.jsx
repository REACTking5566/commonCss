/* eslint-disable linebreak-style */
import React, { useState } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import i18n from "../../i18n";

const StyleWrapper = styled.div`
  .header__translate {
    position: relative;
    .header__translate-inner {
      display: ${({ showTranslate }) => (showTranslate ? "block" : "none")};
      position: absolute;
      top: 100%;
      z-index: 2;
      border: 1px solid green;
      background-color: ${({ theme: { color } }) => color["card"]};
      .header__translate-item {
        padding-bottom: 1rem;
        width: 3rem;
        text-align: center;
        border-bottom: 1px solid gray;
      }
    }
  }
`;

function TranslateBtn({ text }) {
  const [showTranslate, setTranslate] = useState(false);
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };
  const hoverTranslate = (show) => {
    setTranslate(show);
  };
  return (
    <StyleWrapper showTranslate={showTranslate}>
      <div className="header__translate">
        <span
          className="header__translate-text"
          onMouseEnter={() => hoverTranslate(true)}
        >
          i18n
        </span>
        <div
          className="header__translate-inner"
          onMouseLeave={() => hoverTranslate(false)}
        >
          <div
            className="header__translate-item"
            onClick={() => changeLanguage("en")}
          >
            <span className="text-lv7">en</span>
          </div>
          <div
            className="header__translate-item"
            onClick={() => changeLanguage("de")}
          >
            <span className="text-lv7">de</span>
          </div>
          <div
            className="header__translate-item"
            onClick={() => changeLanguage("cn")}
          >
            <span className="text-lv7">cn</span>
          </div>
          <div
            className="header__translate-item"
            onClick={() => changeLanguage("tw")}
          >
            <span className="text-lv7">tw</span>
          </div>
        </div>
      </div>
    </StyleWrapper>
  );
}
export default TranslateBtn;
