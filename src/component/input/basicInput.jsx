/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const StyleWrapper = styled.div`
  input {
    /* position: relative; */
    display: block;
    width: 100%;
    min-height: ${({ inputSize, theme: { size } }) => size[inputSize || "md"]};
    border: 0px solid gray;
    text-align: ${({ textAlign }) => textAlign};
    background-color: transparent;
    font-size: 1rem;
    &:focus {
      outline: none;
    }
    ::placeholder {
      color: ${({ color }) => color || "#fff"};
    }
  }

  /* input[type="checkbox"]:checked:after {
    content: "x";
    display: block;
    position: absolute;
    box-sizing: content-box;
    top: 50%;
    left: 50%;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    background-color: #12cbc4;
    width: 16px;
    height: 16px;
    border-radius: 100vh;
    -webkit-transform: translate(-50%, -50%) scale(0);
  } */
`;

function BasicInput({
  placeholder,
  className,
  textAlign,
  color,
  inputType,
  inputSize,
  callback,
}) {
  return (
    <StyleWrapper textAlign={textAlign} color={color} inputSize={inputSize}>
      <input
        type={inputType || "text"}
        placeholder={placeholder}
        className={`bz ${className}`}
        onKeyUp={callback}
      />
    </StyleWrapper>
  );
}
export default BasicInput;
