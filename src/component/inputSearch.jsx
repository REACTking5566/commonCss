/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import BasicInput from "./input/basicInput";
import { Circle } from "../component/withCircle";
const StyleWrapper = styled.div`
  form {
    position: relative;
  }
  .basicInput {
    border-radius: 15px;
    padding: 5px 10px;
  }
  .btnSetion {
    position: absolute;
    right: 5px;
  }
`;
function InputSearch({ text }) {
  return (
    <StyleWrapper>
      <form action="">
        <BasicInput className="basicInput" inputSize="md" />
        <div className="btnSetion abs-y-center">
          <Circle circleSize="md" borderColor="gray">
            <i className="fas fa-search" />
          </Circle>
        </div>
      </form>
    </StyleWrapper>
  );
}
export default InputSearch;
