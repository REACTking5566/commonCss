/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import BasicInput from "../input/basicInput";
import { WithBorder } from "../withBorder";
import { AryItem } from "../../utility/fun";
import ToggleItem from "../toggle/toggleItem";
import AuctionHeader from "../auction/auctionHeader";
import AuctionHistory from "../auction/auctionHistory";
import { get } from "lodash";
// import WebSocket from "ws";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const StyleWrapper = styled.div`
  .current-bid__inner {
    padding: 1rem;
  }
`;

const QuickBidSection = () =>
  AryItem(3)(
    <div className="quick__bid-btn col-md-4  col-xs-4 m-b-10 bz">
      <WithBorder
        padding="0 1rem"
        borderRadius={26}
        bgColor="warning"
        border="1px solid gray"
      >
        <BasicInput placeholder="fckeer" textAlign="center" />
      </WithBorder>
    </div>
  );
const DirectSection = ({ socket }) => {
  const [bidValue, setBidDirect] = useState(null);
  const prevBidValue = usePrevious(bidValue);
  return (
    <>
      <div className="bid__directly-btn col-md-4 col-xs-6 m-b-10 bz">
        <WithBorder
          padding="0 1rem"
          borderRadius={26}
          bgColor="light"
          border="1px solid gray"
          relative="relative"
        >
          <div className="bid__directly-icon ps-ab abs-y-center">
            <i className="fas fa-dollar-sign"></i>
          </div>
          <BasicInput
            placeholder="fcirday"
            textAlign="center"
            callback={(event) => {
              setBidDirect(event.currentTarget.value);
            }}
          />
        </WithBorder>
      </div>
      <div
        className="bid__directly-btn col-md-6 col-xs-6 m-b-10 bz"
        onClick={() => {
          if (bidValue && prevBidValue !== bidValue) {
            socket.emit("my other event", {
              bidCurrency: bidValue,
              socket_id: socket.id,
              status: null,
            });
          }
        }}
      >
        <WithBorder
          padding="5px 1rem"
          borderRadius={26}
          bgColor="warning"
          border="1px solid gray"
          textAlign="center"
        >
          Pleace Bid
        </WithBorder>
      </div>
    </>
  );
};

const AutoBidSection = ({ socket }) => {
  const [quickBidValue, setQuickBid] = useState(null);
  return (
    <>
      <div className="bid__auto-btn col-md-4 col-xs-6 m-b-10 bz">
        <WithBorder
          padding="0 1rem"
          borderRadius={26}
          bgColor="light"
          border="1px solid gray"
          relative="relative"
        >
          <div className="bid__auto-icon ps-ab abs-y-center">
            <i className="fas fa-dollar-sign"></i>
          </div>
          <BasicInput
            placeholder="fcirday"
            textAlign="center"
            callback={(event) => {
              setQuickBid(event.currentTarget.value);
            }}
          />
        </WithBorder>
      </div>
      <div
        className="bid__auto-btn col-md-6 col-xs-6 m-b-10 bz"
        onClick={() => {
          if (quickBidValue) {
            socket.emit("my other event", { my: quickBidValue });
          }
        }}
      >
        <WithBorder
          padding="5px 1rem"
          borderRadius={26}
          bgColor="info"
          border="1px solid gray"
          textAlign="center"
        >
          Quick-Bid
        </WithBorder>
      </div>
    </>
  );
};

function BidDetialWrapper({ text, socket, bidHistoryData }) {
  return (
    <StyleWrapper>
      <div className="quick-bid__inner">
        <AuctionHeader
          title={get(bidHistoryData[0], "message", "")}
          auctionText="close in"
          BgColor="info"
        />
        <div className="current-bid">
          <div className="current-bid__inner bz">
            <div className="current-bid__amount">
              <span className="text-lv5">#1000</span>
            </div>
            <div className="current-bid__label">
              <a href="">Expert's estimate</a>
            </div>
            <div className="current-bid__estimate">
              <div className="experts__estimate">Expert's estimate</div>
              <div className="great-time-to-buy">
                <strong className="title text-lv5">Experts estimate</strong>
                <i className="fas fa-search" />
                <br />
                <span className="message text-lv7">Experts estimate</span>
              </div>
            </div>
          </div>
        </div>
        <div className="quick__bid">
          <div className="d-flex flex-wrap">
            <div className="quick__bid-title m-b-10 col-md-12">
              <h3 className="text-lv6">quick-bid</h3>
            </div>
            <QuickBidSection socket={socket} />
          </div>
        </div>
        <div className="bid__directly">
          <div className="d-flex flex-wrap">
            <div className="bid__directly-title m-b-10 col-md-12">
              <h3 className="text-lv6">quick-bid</h3>
            </div>
            <DirectSection socket={socket} />
          </div>
        </div>
        <div className="bid__auto">
          <div className="d-flex flex-wrap">
            <div className="bid__auto-title m-b-10 col-md-12">
              <h3 className="text-lv6">auto-bid</h3>
            </div>
            <AutoBidSection socket={socket} />
          </div>
        </div>
        <div className="p-l-15 p-r-15 bz">
          <ToggleItem toggleTitle="historyList">
            <AuctionHistory bidHistoryData={bidHistoryData} />
          </ToggleItem>
        </div>
      </div>
    </StyleWrapper>
  );
}

export default BidDetialWrapper;
