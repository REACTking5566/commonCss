/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import BasicInput from "../input/basicInput";
import { WithBorder } from "../withBorder";
import { AryItem, timeCountDown } from "../../utility/fun";
import BidDetialWrapper from "../bid/bidDetialWrapper";
import { ToggleItem } from "../toggle/toggleItem";
import socketIOClient from "socket.io-client";
// import WebSocket from "ws";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

// const socket = socketIOClient.connect("http://localhost/");

// let ws = typeof window !== "undefined" && new WebSocket("ws://localhost:3000");
// if (typeof window !== "undefined") {
//   ws.onopen = () => {
//     console.log("open connection");
//     // ws.send("5678");
//   };
//   ws.onmessage = (event) => {
//     console.log(event.data);
//   };

//   ws.onclose = () => {
//     console.log("close connection");
//   };
// }
const StyleWrapper = styled.div``;
function AuctionWrapper({ text }) {
  const [updateTime, handleUpdateTime] = useState(0);
  const [socketMessage, setSocketMessage] = useState([]);
  const timerToClearSomewhere = useRef(false); //now you can pass timer to another component
  useEffect(() => {
    if (!updateTime) {
      // timerToClearSomewhere.current = setInterval(() => {
      //   [90, 900, 9600, 500, 10000].map((i, index) =>
      //     timeCountDown(i, document.querySelector(`.countDown_card_${index}`))
      //   );
      //   // timeCountDown(updateTime);
      // }, 1000);
    }
    if (updateTime) {
      setTimeout(() => {
        clearInterval(timerToClearSomewhere.current);
        // timerToClearSomewhere.current = setInterval(() => {
        //   timeCountDown(updateTime);
        // }, 1000);
      }, 500);
    }
  }, [updateTime]);

  useEffect(() => {
    // socket.on("message", (data) => {
    //   setSocketMessage((prev) => [data, ...prev]);
    //   handleUpdateTime((prev) => prev + 90);
    // });
  }, []);
  return (
    <WithBorder bgColor="light" boxShadow="0 0.25rem 0.5rem rgba(34,34,34,0)">
      <div className="quick-bid__section">
        {/* <BidDetialWrapper socket={socket} bidHistoryData={socketMessage} /> */}
      </div>
      <div className="p-l-15 p-r-15 bz">
        <time />
      </div>
    </WithBorder>
  );
}
export default AuctionWrapper;
