/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const StyleWrapper = styled.div`
  .auction__inner {
    background-color: ${({ BgColor, theme: { color } }) => color[BgColor]};
  }
`;

function AuctionHeader({ BgColor, title, subTitle, auctionText, time }) {
  return (
    <StyleWrapper BgColor={BgColor}>
      <div className="auction__Header">
        <div className="auction__inner">
          <div className="auction__timeer text-center">
            <strong className="text-lv5">{auctionText} </strong>
            <span>11d</span>
            <span>11h</span>
            <span>11s</span>
          </div>
          <h1 className="auction__title text-lv5 text-center">{title}</h1>
          <h3 className="auction__subTitle text-lv7 text-center">{subTitle}</h3>
        </div>
      </div>
    </StyleWrapper>
  );
}
export default AuctionHeader;
