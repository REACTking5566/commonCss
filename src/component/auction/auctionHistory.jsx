/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { get, isEmpty } from "lodash";
const StyleWrapper = styled.div`
  .auction__inner {
    background-color: ${(props) => props.BgColor};
  }
`;

function AuctionHistory({ bidHistoryData, BgColor }) {
  return (
    <StyleWrapper>
      <div className="bid__history">
        <div className="bid_history-inner">
          <table>
            <tbody>
              {!isEmpty(bidHistoryData) && (
                <tr>
                  <td className="bid__history-name text-lv8 text-left">user</td>
                  <td className="bid__history-time text-lv8 text-left">
                    price
                  </td>
                  <td className="bid__history-price text-lv8 text-right">
                    time
                  </td>
                </tr>
              )}
              {bidHistoryData.map((data) => (
                <tr>
                  <td className="bid__history-name text-lv8 text-left">
                    {get(data, "id", "")}
                  </td>
                  <td className="bid__history-price text-lv8 text-left">
                    {get(data, "price", "")}
                  </td>
                  <td className="bid__history-time text-lv8 text-right">
                    {get(data, "time", "")}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </StyleWrapper>
  );
}
export default AuctionHistory;
