/* eslint-disable linebreak-style */
import React, { useState } from 'react';
import styled from 'styled-components';
// import Menu, { SubMenu, MenuItem } from 'rc-menu';

const AdStyleWrapper = styled.div`
  .ad--content {
    background-image: url('https://www.spaceadvisor.com/assets/front_v2/img/home/other-info/blog_800_800.jpg');
    position: relative;
    /* padding-bottom: 100%; */
    color: black;
    text-align: center;
    display: block;
    color: #fff;
  }
  .ad--info {
    /* position: absolute;
    bottom: 5px;
    left: 50%;
    transform: translateX(-50%); */
    /* color: #fff; */
  }
  .one {
    height: 200px;
    width: 200px;
    background: purple;
    overflow: hidden;
    position: relative;
    padding: 100px;
    border: 1px solid green;
  }
  .itemWrapper {
    /* display: inline-block; */
    height: 100px;
    /* width: 500px; */
    position: absolute;
    background: red;
    left: ${({ padding }) => padding}px;
    transition-timing-function: ease-in-out;
    /* Quick on the way out */
    transition: 0.35s;
  }
  .lieghtBox--item {
    width: 200px;
    height: auto;
    background: blue;
    border: 1px solid pink;
    position: absolute;
    font-size: 50px;
    color: #fff;
    /* float: left; */
    /* display: inline-block; */
  }
`;

const HotelStyleWrapper = styled.div`
  #hotel {
    .hotel--inner {
      .hotel--img {
        img {
          width: 100%;
          height: auto;
        }
      }
      .hotel--spec {
        align-items: center;
        justify-content: space-between;
        .level {
          padding: 5px 15px;
          background: greenyellow;
        }
      }
      .hotel--price {
        justify-content: space-between;
      }
    }
  }
`;

export const Hotel = () => (
  <HotelStyleWrapper>
    <section id="hotel">
      <div className="hotel--inner">
        <div className="hotel--title"></div>
        <div className="hotel--img">
          <img
            src="https://file.spaceadvisor.com/upload/space_000911/product_img/I48zilKfBcRLsk0VgLY6U0EjevOubvcn.webp"
            alt=""
          />
        </div>
        <div className="hotel--location p-b-10">
          <span className="city">台中市</span>
          <span className="category">教室</span>
        </div>
        <div className="hotel--name p-b-10">
          台中市大墩生活美學協會 - 全場域
        </div>
        <div className="hotel--spec d-flex p-b-10">
          <div className="level">8.6</div>
          <div className="user--view fas fa-eye"> 4129</div>
        </div>
        <div className="hotel--price d-flex">
          <div className="unit">
            <span className="text-lv4">/價錢</span>
          </div>
          <div className="cost">
            <span className="number text-lv5">3000</span>
            <span className="perTime">/3小時起</span>
          </div>
        </div>
      </div>
    </section>
  </HotelStyleWrapper>
);

export function Advertising({ describle, price, imgUrl, soldAmount, zoomImg }) {
  const [padding, usePadding] = useState(0);
  return (
    <AdStyleWrapper padding={padding}>
      <button onClick={() => usePadding(padding + 200)}>left</button>
      <button onClick={() => usePadding(padding - 200)}>right</button>
      <div className="one">
        <div className="itemWrapper">
          {new Array(10).fill({}).map((i, index) => (
            <div
              className="lieghtBox--item text-center"
              style={{ left: `${index * 200}px` }}
            >
              {index}
            </div>
          ))}
        </div>
      </div>
      <div className="ad--section">
        <div className="ad--section__inner">
          <div className="d-flex">
            <div className="col-md-6">
              <div className="ad--item">
                <a href="" className="ad--content">
                  <div className="ad--info__title text-lv5 p-b-20">
                    5664343243223r2342212321332343
                  </div>
                  <div className="ad--info__describle text-lv7 p-b-20">
                    56641321efdsfewrwe23243
                  </div>
                  <div className="ad--info__from text-lv7 p-b-20">
                    5664343243223r2342212321332343
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AdStyleWrapper>
  );
}
// export  Advertising;
