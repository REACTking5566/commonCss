/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const CircleWrapper = styled.button`
  width: ${({ circleSize, theme: { size } }) => size[circleSize]};
  height: ${({ circleSize, theme: { size } }) => size[circleSize]};
  border-radius: ${({ circleSize, theme: { size } }) => size[circleSize]};
  border: 1px solid ${(props) => props.borderColor};
`;

export const Circle = ({ borderColor, circleSize, children }) => (
  <CircleWrapper borderColor={borderColor} circleSize={circleSize}>
    {children}
  </CircleWrapper>
);

const StyleTag = styled.div`
  position: relative;
`;
export const Tag = ({ children }) => <StyleTag>{children}</StyleTag>;
