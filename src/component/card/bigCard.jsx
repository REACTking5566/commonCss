/* eslint-disable linebreak-style */
import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Circle } from "../withCircle";
import { WithBorder } from "../withBorder";

const StyleWrapper = styled.div`
  .big-card__content {
    position: relative;
    .content-footer {
      position: absolute;
      bottom: 0%;
    }
    .card__fav-icon {
      position: absolute;
      right: 0;
    }
  }
`;

function BigCard({ describle, price, imgUrl, soldAmount, zoomImg }) {
  return (
    <StyleWrapper>
      <div className="big-card">
        <div className="big-card__inner">
          <div className="row">
            <div className="big-card__image col-md-4 col-xs-12">
              <div className="image-header">
                <img
                  src="https://assets.catawiki.nl/assets/2020/4/29/9/5/8/thumb2_95821899-37e7-43ab-8b76-29948a8e4db7.jpg"
                  alt=""
                />
              </div>
              <div className="image-footer">
                <div className="image-footer__describle">
                  <span className="text-lv7">3r32432</span>
                </div>
              </div>
            </div>
            <div className="big-card__content col-md-8 col-xs-12">
              <div className="card__fav-icon">
                <Circle circleSize="base" borderColor="gray">
                  <div className="fas fa-heart icontest"></div>
                </Circle>
              </div>
              <div className="content-header">
                <div className="content-header__title">
                  <h3 className="text-lv5">werqreqewr</h3>
                </div>
                <div className="content-header__describle">
                  <span className="text-lv7">werewr</span>
                </div>
              </div>
              <div className="content-footer">
                <div className="d-flex">
                  <div className="content-footer__price">
                    <span className="text-lv5">100</span>
                  </div>
                  <div className="content-footer__lot-list">
                    <WithBorder borderRadius={26} textAlign="center">
                      5566
                    </WithBorder>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </StyleWrapper>
  );
}
export default BigCard;
