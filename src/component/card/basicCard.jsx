/* eslint-disable react/prop-types */
/* eslint-disable linebreak-style */
import React from 'react';
// import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Circle } from '../withCircle';
import { WithBorder } from '../withBorder';

const StyleWrapper = styled.div`
  .card__inner {
    padding: 0 15px;
    position: relative;
    .card__img {
      overflow: hidden;
      img {
        width: 100%;
        height: auto;
      }
    }
    .card__fav-icon {
      position: absolute;
      right: 1rem;
      top: 0;
      z-index: 2;
    }
    .card__content {
    }
    .card__price {
      font-weight: bold;
    }
    .card__status {
      .sold {
        color: #9e9b9b;
      }
    }
  }
`;

function BasicCard({ describle, price, imgUrl, soldAmount, zoomImg }) {
  return (
    <StyleWrapper>
      <div className="card__item">
        <div className="card__inner">
          <div className="card__fav-icon">
            <Circle circleSize="base" borderColor="gray">
              <div className="fas fa-heart icontest"></div>
            </Circle>
          </div>
          <div className="card__img m-b-10">
            <WithBorder>
              <img src={imgUrl} alt="" className={zoomImg ? 'zoom' : ''} />
            </WithBorder>
          </div>
          <div className="card__content">
            {describle && (
              <div className="card__content-header">
                <div className="card__describle m-b-10">
                  <h3 className="text-break text-lv6">{describle}</h3>
                </div>
              </div>
            )}
            {soldAmount && (
              <div className="card__content-footer d-flex justify-between flex-wrap">
                <div className="card__price">
                  <span className="card__price-unit text-lv8">$</span>
                  <span className="card__price-number text-v-8">{price}</span>
                </div>
                <div className="card__status">
                  <span className="text-lv8 sold">已售出{soldAmount}組</span>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </StyleWrapper>
  );
}
export default BasicCard;
