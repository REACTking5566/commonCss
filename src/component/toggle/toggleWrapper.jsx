import React from "react";
const ToggleItem = ({ toggle, content }) => {
  const [showModal, setShowModal] = React.useState(false);
  const hide = () => setShowModal(false);
  const show = () => setShowModal(true);
  return (
    <>
      {toggle(show, hide, showModal)}
      {content(showModal)}
    </>
  );
};

export default ToggleItem;
