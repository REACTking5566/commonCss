import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import ToggleWrapper from "./toggleWrapper";
import { Collapse } from "reactstrap";
import classnames from "classnames";

const ToggleItem = ({ toggleTitle, children }) => (
  <ToggleWrapper
    toggle={(show, hide, showModal) => (
      <div
        className="filter__dropDown"
        onClick={() => {
          if (showModal) {
            hide();
          } else {
            show();
          }
        }}
      >
        <div className="filter__item d-flex justify-between justify-center">
          <span className="filter__item-name text-lv7">{toggleTitle}</span>
          <span className="filter__item-icon">
            <i
              className={`${classnames(
                "fas",
                { "fa-caret-right": !showModal },
                { "fa-caret-down": showModal }
              )}`}
            />
          </span>
        </div>
      </div>
    )}
    content={(showModal) => <Collapse isOpen={showModal}>{children}</Collapse>}
  />
);

export default ToggleItem;
