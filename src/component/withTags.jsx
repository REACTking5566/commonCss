/* eslint-disable linebreak-style */
import React, { useEffect, useState, Fragment, useRef } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

const CardWrapper = styled.div`
  position: relative;
  .card__icon {
    position: absolute;
    left: ${({ left }) => left}px;
    top: ${({ top }) => top}px;
    width: ${({ width }) => width}%;
    z-index: 2;
    color: ${({ tagTextColor }) => tagTextColor || "#fff"};
    background: ${({ tagBgColor }) => tagBgColor || "#424257"};
    span {
      display: inline-block;
      padding: ${({ padding }) => padding};
    } 
    &:after {
      content: "";
      display: ${({ tagStyle }) =>
        tagStyle === "triangle" ? "block" : "none"};
      position: absolute;
      left: 0;
      top: 100%;
      width: 0;
      height: 0;
      border-left: 15px solid transparent;
      border-top: 15px solid #424257;
    }
  }
`;

export const CardTag = ({
  left,
  top,
  children,
  width,
  text,
  tagTextColor,
  tagBgColor,
  tagStyle,
  padding,
}) => (
  <CardWrapper
    left={left}
    top={top}
    width={width}
    tagTextColor={tagTextColor}
    tagBgColor={tagBgColor}
    tagStyle={tagStyle}
    padding={padding}
  >
    <div className="card__icon text-center">
      <span className="text-lv-5">{text}</span>
    </div>
    {children}
  </CardWrapper>
);

const StyleTag = styled.div`
  position: relative;
`;
export const Tag = ({ children }) => <StyleTag>{children}</StyleTag>;
