import ReactDOM from 'react-dom';
import React from 'react';
// eslint-disable-next-line no-unused-vars
import Index from './container/page';
import { ThemeProvider } from 'styled-components';
import theme from './utility/theme';
import './css/common.scss';
import "bootstrap/dist/css/bootstrap.min.css";
// import './i18n'; 
ReactDOM.render(<ThemeProvider theme={theme}><Index /></ThemeProvider>, document.getElementById('root'))
// export default <ThemeProvider theme={theme}><Index /></ThemeProvider>
// export default <div>1231</div>