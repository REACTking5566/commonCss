import moment from 'moment-timezone';

export const AryItem = (size) => (Ele) => Array(size).fill({}).map(() => Ele);
export const timeCountDown = (seconds, dom) => {
  var timeElement, eventTime, currentTime, duration, interval, intervalId;
  interval = 1000; // 1 second
  // get time element
  // calculate difference between two times
  eventTime = moment.tz('2020-06-15T09:00:00', 'Asia/Taipei');
  // based on time set in user's computer time / OS
  currentTime = moment.tz();
  // get duration between two times
  duration = moment.duration(eventTime.add(seconds, 'seconds').diff(currentTime));
  timeElement = dom;

  // loop to countdown every 1 second
  const intervalInstance = function () {
    // get updated duration
    duration = moment.duration(duration - interval, 'milliseconds');
    // if duration is >= 0
    if (duration.asSeconds() <= 0) {
      clearInterval(intervalId);
      // hide the countdown element
      timeElement.classList.add('hidden');
    } else {
      // otherwise, show the updated countdown
      if (timeElement) {
        timeElement.innerText = duration.days() + ' days ' + duration.hours() + ' hours ' + duration.minutes() + ' minutes ' + duration.seconds() + ' seconds';
      }
    }
  };
  return intervalInstance();
  // clear && clearInterval(intervalInstance);
};