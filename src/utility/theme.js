const color = {
  primary: '#1976D2',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107',
  card: '#f8f1db',
  transparent: 'transparent',
  light: '#fcfcfc',
  dark: '#343a40',
};
const base = 2;
const size = {
  xl: `${1.875 * base}rem`,
  lg: `${1.375 * base}rem`,
  md: `${1.25 * base}rem`,
  base: `${1 * base}rem`,
  sm: `${0.875 * base}rem`,
};
export default { size, color };