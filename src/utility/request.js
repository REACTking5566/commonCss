// useRequest.js
import { useState, useEffect } from 'react';
// instance 为myAxios中暴露的添加了默认方法及拦截器的axios实例
import instance, { AXIOS_METHOD_TYPE } from './axios';

/** 方法说明
 * @method useRequest
 * @return [
        responseData, // 请求成功的返回
        errorData, // 请求失败返回
        loading, // 请求是否完成
        setRequestParams // 参数改变重新请求
    ] 
*/
const useRequest = (url, method = AXIOS_METHOD_TYPE.GET, params = {}, config = {}) => {
  method = method.toLocaleUpperCase();
  const data = method === 'GET' ? 'params' : 'data';
  const [responseData, setResponseData] = useState({});
  const [errorData, setErrorData] = useState({});
  const [loading, setLoading] = useState(true);
  const [requestParams, setRequestParams] = useState(params);
  let headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  };
  if (config.headers) {
    headers = {
      ...headers,
      ...config.headers
    };
  }
  useEffect(() => {
    if (!requestParams) return;
    instance(
      url,
      method,
      // [data]: requestParams,
      requestParams,
      headers
    )
      .then(({ data }) => {
        setLoading(false);
        setResponseData(data);
      })
      .catch(err => {
        setLoading(false);
        setErrorData(err);
      });
  }, [requestParams]);
  return [
    responseData,
    errorData,
    loading,
    setRequestParams
  ];
};
export default useRequest;

