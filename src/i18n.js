import i18n from 'i18next';
import detector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
import EN from '../public/locales/en/translation.json';
import DE from '../public/locales/de/translation.json';
import CN from '../public/locales/zh-CN/translation.json';
import TW from '../public/locales/zh-TW/translation.json';
// the translations
const resources = {
  en: {
    translation: EN
  },
  de: {
    translation: DE
  },
  cn: {
    translation: CN
  },
  tw: {
    translation: TW
  }
};

i18n
  .use(detector)
  .use(reactI18nextModule) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;