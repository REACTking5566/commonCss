/* eslint-disable linebreak-style */
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { withNamespaces } from 'react-i18next';
import request from '../utility/axios';
import useRequest from '../utility/request';
import axios from 'axios';
import { AryItem } from '../utility/fun';
import theme from '../utility/theme';
import InputSearch from '../component/inputSearch';
import BasicCard from '../component/card/basicCard';
import BigCard from '../component/card/bigCard';
import AuctionWrapper from '../component/auction/auctionWrapper';
import LazyLoadWrapper from '../component/lazyLoadWrapper';
import { CardTag } from '../component/withTags';
import { WithBorder } from '../component/withBorder';
import SlickWrapper from '../component/slickWrapper';
import LightBoxWrapper from '../component/lightBoxWrapper';
import BasicInput from '../component/input/basicInput';
import BasicFilter from '../component/filter/basicFilter';
import ToggleItem from '../component/toggle/toggleItem';
import BasicTab from '../component/tab/basicTab';
import TabItem from '../component/tab/tabItem';
import MainHeader from '../component/mainHeader';
import MainBanner from '../component/mainBanner';
import BasicAlert from '../component/alert/basicAlert';
import Test from '../component/test';
import { Hotel, Advertising } from '../component/block/ad';
import Menu, { SubMenu, MenuItem } from 'rc-menu';

function handleAxios() {
  if (typeof window !== 'undefined') {
    // let url = "https://www.omdbapi.com/?apikey=ecac3b09&s=pig&page=1";
    let url = 'http://localhost:3000/login';
    var instance = axios.create({
      withCredentials: true,
      params: {
        name: 'chyingp',
        password: '123456',
        page: 1,
      },
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    });
    axios
      .post(url, {
        headers: {
          withCredentials: true,
        },
        params: {
          name: 'chyingp',
          password: '123456',
          page: 1,
        },
      })
      .then((res) => {})
      .catch((err) => {});
  }
}

const funA = () =>
  axios.get('https://www.omdbapi.com/?apikey=ecac3b09&s=pig&page=1');
const funB = () => axios.get('https://rickandmortyapi.com/api/character/');
function Index({ text, t }) {
  const { color } = theme;
  const [imageList, setImage] = useState([]);
  useEffect(() => {
    handleAxios();
    axios
      .get('https://rickandmortyapi.com/api/character/?page=4')
      .then((response) => {
        setImage(response.data.results);
      });
    axios.all([funA(), funB()]).then(
      axios.spread((a, b) => {
        console.table('FuncA 回傳結果', a.data);
        console.table('FuncB 回傳結果', b.data);
      })
    );
    axios
      .get('http://localhost:3000/', {
        headers: { 'Content-Type': 'application/json' },
        params: { ID: 123 },
        withCredentials: true,
        maxContentLength: 10,
      })
      .then((response) => {
        // setImage(response.data.results);
      });
    // console.log(gg);
  }, []);
  const [responseData, errorData, loading, setRequestParams] = useRequest(
    'https://rickandmortyapi.com/api/character/',
    'get'
  );
  return (
    <div className="p-l-15 p-r-15 bz">
      {/* <Menu className="rc-menu-cust">
        <MenuItem>1</MenuItem>
        <SubMenu title="3" className="rc-submenu-cust">
          <MenuItem className="rc-submenuItem-cust">2-1</MenuItem>
          <MenuItem className="rc-submenuItem-cust">2-3</MenuItem>
          <MenuItem className="rc-submenuItem-cust">2-11311</MenuItem>
          <MenuItem className="rc-submenuItem-cust">2-23</MenuItem>
        </SubMenu>
      </Menu> */}
      <div className="d-flex">
        {new Array(5).fill({}).map(() => (
          <div className="col-md-2">
            <Hotel />
          </div>
        ))}
      </div>
      <Advertising />
      <div
        onClick={() => setRequestParams({ page: 5 })}
        style={{ paddingTop: '50px', height: '100px' }}
      >
        clike mememe
      </div>
      <h1 className="animate__animated animate__bounce">An animated element</h1>
      <div className="col-xs-none elementToFadeInAndOut">
        <BasicAlert />
      </div>
      <Test />
      <div onClick={handleAxios}>press emse ssid</div>
      <BasicTab row={true}>
        <TabItem>324</TabItem>
        <TabItem>324</TabItem>
        <TabItem>9999</TabItem>
      </BasicTab>
      <div className="row">
        {new Array(12).fill({}).map((_, index) => (
          <div className="col-md-6 col-xs-12" key={`bigCard_${index}`}>
            <BigCard />
          </div>
        ))}
      </div>
      <h1>{t('Currently the highest bidder')}</h1>
      <MainHeader color={color} />
      <div className="m-t-20" />
      <LazyLoadWrapper>
        <MainBanner mainBannerBgUrl="https://cdn.catawiki.net/assets/buyer/ui/v2/homepage/hero/hero-small@x1-8e50f93981fe22f615f506384a9a764e5d4ed0d8d3a76ecb0804bc6c977460f8.jpg" />
      </LazyLoadWrapper>
      <div className="m-t-20" />
      <div className="pop-this-week">
        <div className="pop-this-week__inner">
          <div className="pop-this-week__title">
            <h3>pop this week</h3>
          </div>
          <div className="pop-this-week__cards">
            <SlickWrapper maxWidth="1600PX">
              {Array(15)
                .fill({})
                .map((i, index) => (
                  <div className="bz m-b-20" key={`cardTag_${index}`}>
                    <CardTag
                      width={50}
                      text="popluar"
                      left="15"
                      top="0"
                      padding="5px 0"
                    >
                      <BasicCard
                        describle="werewrewsdfsdfsdfsdfewrewrewsdfdsfs"
                        zoomImg={true}
                        imgUrl="https://assets.catawiki.nl/assets/2019/10/14/8/c/2/thumb2_8c2b215d-0397-426c-ab99-03b6ea8a64a3.jpg"
                      />
                      <time className={`countDown_card_${index} p-l-15`} />
                    </CardTag>
                  </div>
                ))}
            </SlickWrapper>
          </div>
        </div>
      </div>
      <div className="m-t-20" />
      <div className="d-flex flex-wrap">
        <div className="col-md-4 col-xs-12">
          <ToggleItem toggleTitle="Apple">
            <div className="d-flex justify-between">
              <BasicFilter text="ewrewrewrewwr" />
              <span className="filter__checkbox-count">32423</span>
            </div>
          </ToggleItem>
        </div>
        <div className="col-md-8 col-xs-12 bz">
          <AuctionWrapper />
        </div>
      </div>
      <div className="m-t-20" />
      <div className="col-xs-12 col-md-5 bz">
        <WithBorder
          padding="3px"
          borderRadius={26}
          border={`1px solid ${color['info']}`}
        >
          <InputSearch />
        </WithBorder>
      </div>
      <div className="m-t-20" />
      <div className="d-flex flex-wrap">
        {imageList.map((item, index) => (
          <div className="col-md-4 col-xs-12" key={`LazyLoadWrapper_${index}`}>
            <LazyLoadWrapper>
              <BasicCard imgUrl={item.image} />
            </LazyLoadWrapper>
          </div>
        ))}
      </div>
    </div>
  );
}
export default withNamespaces()(Index);
