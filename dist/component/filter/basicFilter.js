"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withCircle = require("../withCircle");

var _withBorder = require("../withBorder");

var _basicInput = _interopRequireDefault(require("../input/basicInput"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .filter__checkbox {\n    min-width: 1rem;\n  }\n"])));

function BasicFilter(_ref) {
  var text = _ref.text;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: "",
    className: "filter__label d-flex align-item-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "filter__checkbox m-r-15"
  }, /*#__PURE__*/_react["default"].createElement(_basicInput["default"], {
    inputType: "checkbox",
    inputSize: "sm"
  })), /*#__PURE__*/_react["default"].createElement("span", {
    className: "filter__text"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv6"
  }, text))));
}

var _default = BasicFilter;
exports["default"] = _default;