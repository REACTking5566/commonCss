"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _inputSearch = _interopRequireDefault(require("../component/inputSearch"));

var _withBorder = require("../component/withBorder");

var _toggleItem = _interopRequireDefault(require("../component/toggle/toggleItem"));

var _basicBtn = _interopRequireDefault(require("../component/button/basicBtn"));

var _TranslateBtn = _interopRequireDefault(require("../component/i18n/TranslateBtn"));

var _reactstrap = require("reactstrap");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  @media (max-width: 768px) {\n    .main-header__search,\n    .main-header__help,\n    .main-header__sell,\n    .main-header__sign {\n      display: none;\n    }\n  }\n  @media (min-width: 768px) {\n    .main-header__search-m-logo {\n      display: none;\n    }\n    .main-header__sign-m {\n      display: none;\n    }\n  }\n  .main-header__classic-logo {\n    min-height: 3rem;\n    align-items: center;\n    background-color: ", ";\n    color: #fff;\n    padding: 0 1rem;\n  }\n  .main-header__help {\n    span {\n      color: ", ";\n    } \n  }\n"])), function (_ref) {
  var color = _ref.theme.color;
  return color["info"];
}, function (_ref2) {
  var color = _ref2.theme.color;
  return color["dark"];
});

var MainHeader = function MainHeader(_ref3) {
  var bgColor = _ref3.bgColor,
      overflowHideen = _ref3.overflowHideen,
      borderRadius = _ref3.borderRadius,
      children = _ref3.children,
      padding = _ref3.padding,
      border = _ref3.border,
      relative = _ref3.relative,
      textAlign = _ref3.textAlign,
      color = _ref3.color;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showMobileHeader = _useState2[0],
      setMobileHeader = _useState2[1];

  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-wrap align-item-center justify-between"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__classic-logo d-flex"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv7"
  }, "\u7AF6\u6A19\u7DB2\u7AD9")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__categories"
  }, /*#__PURE__*/_react["default"].createElement(_toggleItem["default"], {
    toggleTitle: "cartegories"
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__search col-md-5 col-xs-5"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "3px",
    borderRadius: 26,
    border: "1px solid ".concat(color["info"])
  }, /*#__PURE__*/_react["default"].createElement(_inputSearch["default"], null))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__search-m-logo",
    onClick: function onClick() {
      return setMobileHeader(!showMobileHeader);
    }
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-search"
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__help"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: ""
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "help"))), /*#__PURE__*/_react["default"].createElement(_TranslateBtn["default"], null), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__favorite"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-heart"
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__sign"
  }, /*#__PURE__*/_react["default"].createElement(_basicBtn["default"], null, "SIGN IN")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__sign-m"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-user-alt"
  }))), /*#__PURE__*/_react["default"].createElement(_reactstrap.Collapse, {
    isOpen: showMobileHeader
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-header__search-m col-xs-12"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "3px",
    borderRadius: 26,
    border: "1px solid ".concat(color["info"])
  }, /*#__PURE__*/_react["default"].createElement(_inputSearch["default"], null)))))));
};

var _default = MainHeader;
exports["default"] = _default;