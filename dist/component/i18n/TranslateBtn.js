"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _i18n = _interopRequireDefault(require("../../i18n"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .header__translate {\n    position: relative;\n    .header__translate-inner {\n      display: ", ";\n      position: absolute;\n      top: 100%;\n      z-index: 2;\n      border: 1px solid green;\n      background-color: ", ";\n      .header__translate-item {\n        padding-bottom: 1rem;\n        width: 3rem;\n        text-align: center;\n        border-bottom: 1px solid gray;\n      }\n    }\n  }\n"])), function (_ref) {
  var showTranslate = _ref.showTranslate;
  return showTranslate ? "block" : "none";
}, function (_ref2) {
  var color = _ref2.theme.color;
  return color["card"];
});

function TranslateBtn(_ref3) {
  var text = _ref3.text;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showTranslate = _useState2[0],
      setTranslate = _useState2[1];

  var changeLanguage = function changeLanguage(lng) {
    _i18n["default"].changeLanguage(lng);
  };

  var hoverTranslate = function hoverTranslate(show) {
    setTranslate(show);
  };

  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, {
    showTranslate: showTranslate
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "header__translate-text",
    onMouseEnter: function onMouseEnter() {
      return hoverTranslate(true);
    }
  }, "i18n"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate-inner",
    onMouseLeave: function onMouseLeave() {
      return hoverTranslate(false);
    }
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate-item",
    onClick: function onClick() {
      return changeLanguage("en");
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "en")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate-item",
    onClick: function onClick() {
      return changeLanguage("de");
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "de")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate-item",
    onClick: function onClick() {
      return changeLanguage("cn");
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "cn")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "header__translate-item",
    onClick: function onClick() {
      return changeLanguage("tw");
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "tw")))));
}

var _default = TranslateBtn;
exports["default"] = _default;