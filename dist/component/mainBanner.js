"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _inputSearch = _interopRequireDefault(require("../component/inputSearch"));

var _withBorder = require("../component/withBorder");

var _toggleItem = require("../component/toggle/toggleItem");

var _basicBtn = _interopRequireDefault(require("../component/button/basicBtn"));

var _TranslateBtn = _interopRequireDefault(require("../component/i18n/TranslateBtn"));

var _reactstrap = require("reactstrap");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .main-banner__img {\n    width: 100%;\n    height: auto;\n  }\n  .main-banner {\n    min-height: 5rem;\n    width: 100%;\n    .main-banner__inner {\n      position: relative;\n      height: 100%;\n      text-align: center;\n      span {\n        color: ", ";\n      }\n    }\n    .main-banner__describle {\n      position: absolute;\n    }\n    .main-banner__bg {\n      background-image: ", ";\n      height: 300px;\n    }\n  }\n"])), function (_ref) {
  var color = _ref.theme.color;
  return color["light"];
}, function (_ref2) {
  var mainBannerBgUrl = _ref2.mainBannerBgUrl;
  return "url(".concat(mainBannerBgUrl, ")");
});

var MainBanner = function MainBanner(_ref3) {
  var mainBannerBgUrl = _ref3.mainBannerBgUrl;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showMobileHeader = _useState2[0],
      setMobileHeader = _useState2[1];

  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, {
    mainBannerBgUrl: mainBannerBgUrl
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__bg"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__describle abs-both"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__title m-b-15"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv3"
  }, "Unique Objects")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__subTitle m-b-15"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv6"
  }, "Selected by our experts")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "main-banner__discover"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    bgColor: "warning",
    borderRadius: "26",
    padding: "10px 0"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: ""
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv6"
  }, "discover more")))))))));
};

var _default = MainBanner;
exports["default"] = _default;