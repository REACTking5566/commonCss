"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Advertising = Advertising;
exports.Hotel = void 0;

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// import Menu, { SubMenu, MenuItem } from 'rc-menu';
var AdStyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .ad--content {\n    background-image: url('https://www.spaceadvisor.com/assets/front_v2/img/home/other-info/blog_800_800.jpg');\n    position: relative;\n    /* padding-bottom: 100%; */\n    color: black;\n    text-align: center;\n    display: block;\n    color: #fff;\n  }\n  .ad--info {\n    /* position: absolute;\n    bottom: 5px;\n    left: 50%;\n    transform: translateX(-50%); */\n    /* color: #fff; */\n  }\n  .one {\n    height: 200px;\n    width: 200px;\n    background: purple;\n    overflow: hidden;\n    position: relative;\n    padding: 100px;\n    border: 1px solid green;\n  }\n  .itemWrapper {\n    /* display: inline-block; */\n    height: 100px;\n    /* width: 500px; */\n    position: absolute;\n    background: red;\n    left: ", "px;\n    transition-timing-function: ease-in-out;\n    /* Quick on the way out */\n    transition: 0.35s;\n  }\n  .lieghtBox--item {\n    width: 200px;\n    height: auto;\n    background: blue;\n    border: 1px solid pink;\n    position: absolute;\n    font-size: 50px;\n    color: #fff;\n    /* float: left; */\n    /* display: inline-block; */\n  }\n"])), function (_ref) {
  var padding = _ref.padding;
  return padding;
});

var HotelStyleWrapper = _styledComponents["default"].div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  #hotel {\n    .hotel--inner {\n      .hotel--img {\n        img {\n          width: 100%;\n          height: auto;\n        }\n      }\n      .hotel--spec {\n        align-items: center;\n        justify-content: space-between;\n        .level {\n          padding: 5px 15px;\n          background: greenyellow;\n        }\n      }\n      .hotel--price {\n        justify-content: space-between;\n      }\n    }\n  }\n"])));

var Hotel = function Hotel() {
  return /*#__PURE__*/_react["default"].createElement(HotelStyleWrapper, null, /*#__PURE__*/_react["default"].createElement("section", {
    id: "hotel"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--title"
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--img"
  }, /*#__PURE__*/_react["default"].createElement("img", {
    src: "https://file.spaceadvisor.com/upload/space_000911/product_img/I48zilKfBcRLsk0VgLY6U0EjevOubvcn.webp",
    alt: ""
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--location p-b-10"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "city"
  }, "\u53F0\u4E2D\u5E02"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "category"
  }, "\u6559\u5BA4")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--name p-b-10"
  }, "\u53F0\u4E2D\u5E02\u5927\u58A9\u751F\u6D3B\u7F8E\u5B78\u5354\u6703 - \u5168\u5834\u57DF"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--spec d-flex p-b-10"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "level"
  }, "8.6"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "user--view fas fa-eye"
  }, " 4129")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "hotel--price d-flex"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "unit"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv4"
  }, "/\u50F9\u9322")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "cost"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "number text-lv5"
  }, "3000"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "perTime"
  }, "/3\u5C0F\u6642\u8D77"))))));
};

exports.Hotel = Hotel;

function Advertising(_ref2) {
  var describle = _ref2.describle,
      price = _ref2.price,
      imgUrl = _ref2.imgUrl,
      soldAmount = _ref2.soldAmount,
      zoomImg = _ref2.zoomImg;

  var _useState = (0, _react.useState)(0),
      _useState2 = _slicedToArray(_useState, 2),
      padding = _useState2[0],
      usePadding = _useState2[1];

  return /*#__PURE__*/_react["default"].createElement(AdStyleWrapper, {
    padding: padding
  }, /*#__PURE__*/_react["default"].createElement("button", {
    onClick: function onClick() {
      return usePadding(padding + 200);
    }
  }, "left"), /*#__PURE__*/_react["default"].createElement("button", {
    onClick: function onClick() {
      return usePadding(padding - 200);
    }
  }, "right"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "one"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "itemWrapper"
  }, new Array(10).fill({}).map(function (i, index) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: "lieghtBox--item text-center",
      style: {
        left: "".concat(index * 200, "px")
      }
    }, index);
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--section"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--section__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "col-md-6"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--item"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "",
    className: "ad--content"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--info__title text-lv5 p-b-20"
  }, "5664343243223r2342212321332343"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--info__describle text-lv7 p-b-20"
  }, "56641321efdsfewrwe23243"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "ad--info__from text-lv7 p-b-20"
  }, "5664343243223r2342212321332343"))))))));
} // export  Advertising;