"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _basicInput = _interopRequireDefault(require("../input/basicInput"));

var _withBorder = require("../withBorder");

var _fun = require("../../utility/fun");

var _toggleItem = _interopRequireDefault(require("../toggle/toggleItem"));

var _auctionHeader = _interopRequireDefault(require("../auction/auctionHeader"));

var _auctionHistory = _interopRequireDefault(require("../auction/auctionHistory"));

var _lodash = require("lodash");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// import WebSocket from "ws";
function usePrevious(value) {
  var ref = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    ref.current = value;
  });
  return ref.current;
}

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .current-bid__inner {\n    padding: 1rem;\n  }\n"])));

var QuickBidSection = function QuickBidSection() {
  return (0, _fun.AryItem)(3)( /*#__PURE__*/_react["default"].createElement("div", {
    className: "quick__bid-btn col-md-4  col-xs-4 m-b-10 bz"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "0 1rem",
    borderRadius: 26,
    bgColor: "warning",
    border: "1px solid gray"
  }, /*#__PURE__*/_react["default"].createElement(_basicInput["default"], {
    placeholder: "fckeer",
    textAlign: "center"
  }))));
};

var DirectSection = function DirectSection(_ref) {
  var socket = _ref.socket;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      bidValue = _useState2[0],
      setBidDirect = _useState2[1];

  var prevBidValue = usePrevious(bidValue);
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__directly-btn col-md-4 col-xs-6 m-b-10 bz"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "0 1rem",
    borderRadius: 26,
    bgColor: "light",
    border: "1px solid gray",
    relative: "relative"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__directly-icon ps-ab abs-y-center"
  }, /*#__PURE__*/_react["default"].createElement("i", {
    className: "fas fa-dollar-sign"
  })), /*#__PURE__*/_react["default"].createElement(_basicInput["default"], {
    placeholder: "fcirday",
    textAlign: "center",
    callback: function callback(event) {
      setBidDirect(event.currentTarget.value);
    }
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__directly-btn col-md-6 col-xs-6 m-b-10 bz",
    onClick: function onClick() {
      if (bidValue && prevBidValue !== bidValue) {
        socket.emit("my other event", {
          bidCurrency: bidValue,
          socket_id: socket.id,
          status: null
        });
      }
    }
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "5px 1rem",
    borderRadius: 26,
    bgColor: "warning",
    border: "1px solid gray",
    textAlign: "center"
  }, "Pleace Bid")));
};

var AutoBidSection = function AutoBidSection(_ref2) {
  var socket = _ref2.socket;

  var _useState3 = (0, _react.useState)(null),
      _useState4 = _slicedToArray(_useState3, 2),
      quickBidValue = _useState4[0],
      setQuickBid = _useState4[1];

  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__auto-btn col-md-4 col-xs-6 m-b-10 bz"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "0 1rem",
    borderRadius: 26,
    bgColor: "light",
    border: "1px solid gray",
    relative: "relative"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__auto-icon ps-ab abs-y-center"
  }, /*#__PURE__*/_react["default"].createElement("i", {
    className: "fas fa-dollar-sign"
  })), /*#__PURE__*/_react["default"].createElement(_basicInput["default"], {
    placeholder: "fcirday",
    textAlign: "center",
    callback: function callback(event) {
      setQuickBid(event.currentTarget.value);
    }
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__auto-btn col-md-6 col-xs-6 m-b-10 bz",
    onClick: function onClick() {
      if (quickBidValue) {
        socket.emit("my other event", {
          my: quickBidValue
        });
      }
    }
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    padding: "5px 1rem",
    borderRadius: 26,
    bgColor: "info",
    border: "1px solid gray",
    textAlign: "center"
  }, "Quick-Bid")));
};

function BidDetialWrapper(_ref3) {
  var text = _ref3.text,
      socket = _ref3.socket,
      bidHistoryData = _ref3.bidHistoryData;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "quick-bid__inner"
  }, /*#__PURE__*/_react["default"].createElement(_auctionHeader["default"], {
    title: (0, _lodash.get)(bidHistoryData[0], "message", ""),
    auctionText: "close in",
    BgColor: "info"
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "current-bid"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "current-bid__inner bz"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "current-bid__amount"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv5"
  }, "#1000")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "current-bid__label"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: ""
  }, "Expert's estimate")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "current-bid__estimate"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "experts__estimate"
  }, "Expert's estimate"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "great-time-to-buy"
  }, /*#__PURE__*/_react["default"].createElement("strong", {
    className: "title text-lv5"
  }, "Experts estimate"), /*#__PURE__*/_react["default"].createElement("i", {
    className: "fas fa-search"
  }), /*#__PURE__*/_react["default"].createElement("br", null), /*#__PURE__*/_react["default"].createElement("span", {
    className: "message text-lv7"
  }, "Experts estimate"))))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "quick__bid"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-wrap"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "quick__bid-title m-b-10 col-md-12"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv6"
  }, "quick-bid")), /*#__PURE__*/_react["default"].createElement(QuickBidSection, {
    socket: socket
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__directly"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-wrap"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__directly-title m-b-10 col-md-12"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv6"
  }, "quick-bid")), /*#__PURE__*/_react["default"].createElement(DirectSection, {
    socket: socket
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__auto"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-wrap"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__auto-title m-b-10 col-md-12"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv6"
  }, "auto-bid")), /*#__PURE__*/_react["default"].createElement(AutoBidSection, {
    socket: socket
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "p-l-15 p-r-15 bz"
  }, /*#__PURE__*/_react["default"].createElement(_toggleItem["default"], {
    toggleTitle: "historyList"
  }, /*#__PURE__*/_react["default"].createElement(_auctionHistory["default"], {
    bidHistoryData: bidHistoryData
  })))));
}

var _default = BidDetialWrapper;
exports["default"] = _default;