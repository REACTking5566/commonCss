"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _simpleReactLightbox = require("simple-react-lightbox");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var options = {
  overlayColor: "rgb(25, 136, 124)",
  captionColor: "#a6cfa5",
  captionFontFamily: "Raleway, sans-serif",
  captionFontSize: "22px",
  captionFontWeight: "300",
  captionFontStyle: "capitalize",
  buttonsBackgroundColor: "red",
  buttonsIconColor: "rgba(126, 172, 139, 0.8)",
  autoplaySpeed: 1500,
  transitionSpeed: 900,
  showDownloadButton: true
};

var LightBoxWrapper = function LightBoxWrapper(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement(_simpleReactLightbox.SRLWrapper, {
    options: options
  }, children);
};

var _default = LightBoxWrapper;
exports["default"] = _default;