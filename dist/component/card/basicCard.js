"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withCircle = require("../withCircle");

var _withBorder = require("../withBorder");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .card__inner {\n    padding: 0 15px;\n    position: relative;\n    .card__img {\n      overflow: hidden;\n      img {\n        width: 100%;\n        height: auto;\n      }\n    }\n    .card__fav-icon {\n      position: absolute;\n      right: 1rem;\n      top: 0;\n      z-index: 2;\n    }\n    .card__content {\n    }\n    .card__price {\n      font-weight: bold;\n    }\n    .card__status {\n      .sold {\n        color: #9e9b9b;\n      }\n    }\n  }\n"])));

function BasicCard(_ref) {
  var describle = _ref.describle,
      price = _ref.price,
      imgUrl = _ref.imgUrl,
      soldAmount = _ref.soldAmount,
      zoomImg = _ref.zoomImg;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__item"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__fav-icon"
  }, /*#__PURE__*/_react["default"].createElement(_withCircle.Circle, {
    circleSize: "base",
    borderColor: "gray"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-heart icontest"
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__img m-b-10"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, null, /*#__PURE__*/_react["default"].createElement("img", {
    src: imgUrl,
    alt: "",
    className: zoomImg ? 'zoom' : ''
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__content"
  }, describle && /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__content-header"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__describle m-b-10"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-break text-lv6"
  }, describle))), soldAmount && /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__content-footer d-flex justify-between flex-wrap"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__price"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "card__price-unit text-lv8"
  }, "$"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "card__price-number text-v-8"
  }, price)), /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__status"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv8 sold"
  }, "\u5DF2\u552E\u51FA", soldAmount, "\u7D44")))))));
}

var _default = BasicCard;
exports["default"] = _default;