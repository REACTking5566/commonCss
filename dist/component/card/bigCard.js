"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withCircle = require("../withCircle");

var _withBorder = require("../withBorder");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .big-card__content {\n    position: relative;\n    .content-footer {\n      position: absolute;\n      bottom: 0%;\n    }\n    .card__fav-icon {\n      position: absolute;\n      right: 0;\n    }\n  }\n"])));

function BigCard(_ref) {
  var describle = _ref.describle,
      price = _ref.price,
      imgUrl = _ref.imgUrl,
      soldAmount = _ref.soldAmount,
      zoomImg = _ref.zoomImg;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "big-card"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "big-card__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "row"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "big-card__image col-md-4 col-xs-12"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "image-header"
  }, /*#__PURE__*/_react["default"].createElement("img", {
    src: "https://assets.catawiki.nl/assets/2020/4/29/9/5/8/thumb2_95821899-37e7-43ab-8b76-29948a8e4db7.jpg",
    alt: ""
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "image-footer"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "image-footer__describle"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "3r32432")))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "big-card__content col-md-8 col-xs-12"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__fav-icon"
  }, /*#__PURE__*/_react["default"].createElement(_withCircle.Circle, {
    circleSize: "base",
    borderColor: "gray"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-heart icontest"
  }))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-header"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-header__title"
  }, /*#__PURE__*/_react["default"].createElement("h3", {
    className: "text-lv5"
  }, "werqreqewr")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-header__describle"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv7"
  }, "werewr"))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-footer"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-footer__price"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv5"
  }, "100")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "content-footer__lot-list"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    borderRadius: 26,
    textAlign: "center"
  }, "5566")))))))));
}

var _default = BigCard;
exports["default"] = _default;