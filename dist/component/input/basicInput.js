"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  input {\n    /* position: relative; */\n    display: block;\n    width: 100%;\n    min-height: ", ";\n    border: 0px solid gray;\n    text-align: ", ";\n    background-color: transparent;\n    font-size: 1rem;\n    &:focus {\n      outline: none;\n    }\n    ::placeholder {\n      color: ", ";\n    }\n  }\n\n  /* input[type=\"checkbox\"]:checked:after {\n    content: \"x\";\n    display: block;\n    position: absolute;\n    box-sizing: content-box;\n    top: 50%;\n    left: 50%;\n    -webkit-transform-origin: 50% 50%;\n    transform-origin: 50% 50%;\n    background-color: #12cbc4;\n    width: 16px;\n    height: 16px;\n    border-radius: 100vh;\n    -webkit-transform: translate(-50%, -50%) scale(0);\n  } */\n"])), function (_ref) {
  var inputSize = _ref.inputSize,
      size = _ref.theme.size;
  return size[inputSize || "md"];
}, function (_ref2) {
  var textAlign = _ref2.textAlign;
  return textAlign;
}, function (_ref3) {
  var color = _ref3.color;
  return color || "#fff";
});

function BasicInput(_ref4) {
  var placeholder = _ref4.placeholder,
      className = _ref4.className,
      textAlign = _ref4.textAlign,
      color = _ref4.color,
      inputType = _ref4.inputType,
      inputSize = _ref4.inputSize,
      callback = _ref4.callback;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, {
    textAlign: textAlign,
    color: color,
    inputSize: inputSize
  }, /*#__PURE__*/_react["default"].createElement("input", {
    type: inputType || "text",
    placeholder: placeholder,
    className: "bz ".concat(className),
    onKeyUp: callback
  }));
}

var _default = BasicInput;
exports["default"] = _default;