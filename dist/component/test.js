"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withTags = require("./withTags");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .gay {\n    height: 100px;\n    width: 200px;\n    background: blue;\n    position: relative;\n  }\n  .footer {\n    /* height: 100px;\n    width: 200px; */\n    position: absolute;\n    z-index: -2;\n    border-radius: 6px;\n    background-color: red;\n    border: 1px solid black;\n    &:hover {\n      box-shadow: 0 0 12px 1.5px gray;\n    }\n    /* display: inline-block; */\n  }\n  .bg {\n    height: 20px;\n    /* border: 1px solid green; */\n    padding: 200px;\n  }\n"])));

function Footer(_ref) {
  var text = _ref.text;
  return /*#__PURE__*/_react["default"].createElement(_withTags.Triangle, {
    left: -15
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__icon"
  }), /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "gay"
  }, /*#__PURE__*/_react["default"].createElement("section", {
    className: "footer"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bg"
  }, text)))));
}

var _default = Footer;
exports["default"] = _default;