"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithBorder = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var BorderWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  border-radius: ", "px;\n  background-color: ", ";\n  overflow: ", ";\n  padding: ", ";\n  border: ", ";\n  position: ", ";\n  text-align: ", ";\n  box-shadow: ", ";\n"])), function (_ref) {
  var borderRadius = _ref.borderRadius;
  return borderRadius || 0;
}, function (_ref2) {
  var color = _ref2.theme.color,
      bgColor = _ref2.bgColor;
  return color[bgColor];
}, function (_ref3) {
  var overflowHideen = _ref3.overflowHideen;
  return overflowHideen && 'hidden';
}, function (_ref4) {
  var padding = _ref4.padding;
  return padding || 0;
}, function (_ref5) {
  var border = _ref5.border,
      color = _ref5.theme.color;
  return border || "1px solid ".concat(color['secondary']);
}, function (_ref6) {
  var relative = _ref6.relative;
  return relative;
}, function (_ref7) {
  var textAlign = _ref7.textAlign;
  return textAlign;
}, function (_ref8) {
  var boxShadow = _ref8.boxShadow;
  return boxShadow;
});

var WithBorder = function WithBorder(_ref9) {
  var bgColor = _ref9.bgColor,
      overflowHideen = _ref9.overflowHideen,
      borderRadius = _ref9.borderRadius,
      children = _ref9.children,
      padding = _ref9.padding,
      border = _ref9.border,
      relative = _ref9.relative,
      textAlign = _ref9.textAlign,
      boxShadow = _ref9.boxShadow;
  return /*#__PURE__*/_react["default"].createElement(BorderWrapper, {
    overflowHideen: overflowHideen,
    bgColor: bgColor,
    borderRadius: borderRadius,
    padding: padding,
    border: border,
    relative: relative,
    textAlign: textAlign,
    boxShadow: boxShadow
  }, children);
};

exports.WithBorder = WithBorder;