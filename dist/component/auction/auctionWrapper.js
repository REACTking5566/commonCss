"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _basicInput = _interopRequireDefault(require("../input/basicInput"));

var _withBorder = require("../withBorder");

var _fun = require("../../utility/fun");

var _bidDetialWrapper = _interopRequireDefault(require("../bid/bidDetialWrapper"));

var _toggleItem = require("../toggle/toggleItem");

var _socket = _interopRequireDefault(require("socket.io-client"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// import WebSocket from "ws";
function usePrevious(value) {
  var ref = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    ref.current = value;
  });
  return ref.current;
} // const socket = socketIOClient.connect("http://localhost/");
// let ws = typeof window !== "undefined" && new WebSocket("ws://localhost:3000");
// if (typeof window !== "undefined") {
//   ws.onopen = () => {
//     console.log("open connection");
//     // ws.send("5678");
//   };
//   ws.onmessage = (event) => {
//     console.log(event.data);
//   };
//   ws.onclose = () => {
//     console.log("close connection");
//   };
// }


var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral([""])));

function AuctionWrapper(_ref) {
  var text = _ref.text;

  var _useState = (0, _react.useState)(0),
      _useState2 = _slicedToArray(_useState, 2),
      updateTime = _useState2[0],
      handleUpdateTime = _useState2[1];

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      socketMessage = _useState4[0],
      setSocketMessage = _useState4[1];

  var timerToClearSomewhere = (0, _react.useRef)(false); //now you can pass timer to another component

  (0, _react.useEffect)(function () {
    if (!updateTime) {// timerToClearSomewhere.current = setInterval(() => {
      //   [90, 900, 9600, 500, 10000].map((i, index) =>
      //     timeCountDown(i, document.querySelector(`.countDown_card_${index}`))
      //   );
      //   // timeCountDown(updateTime);
      // }, 1000);
    }

    if (updateTime) {
      setTimeout(function () {
        clearInterval(timerToClearSomewhere.current); // timerToClearSomewhere.current = setInterval(() => {
        //   timeCountDown(updateTime);
        // }, 1000);
      }, 500);
    }
  }, [updateTime]);
  (0, _react.useEffect)(function () {// socket.on("message", (data) => {
    //   setSocketMessage((prev) => [data, ...prev]);
    //   handleUpdateTime((prev) => prev + 90);
    // });
  }, []);
  return /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    bgColor: "light",
    boxShadow: "0 0.25rem 0.5rem rgba(34,34,34,0)"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "quick-bid__section"
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "p-l-15 p-r-15 bz"
  }, /*#__PURE__*/_react["default"].createElement("time", null)));
}

var _default = AuctionWrapper;
exports["default"] = _default;