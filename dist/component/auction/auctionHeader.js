"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .auction__inner {\n    background-color: ", ";\n  }\n"])), function (_ref) {
  var BgColor = _ref.BgColor,
      color = _ref.theme.color;
  return color[BgColor];
});

function AuctionHeader(_ref2) {
  var BgColor = _ref2.BgColor,
      title = _ref2.title,
      subTitle = _ref2.subTitle,
      auctionText = _ref2.auctionText,
      time = _ref2.time;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, {
    BgColor: BgColor
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "auction__Header"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "auction__inner"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "auction__timeer text-center"
  }, /*#__PURE__*/_react["default"].createElement("strong", {
    className: "text-lv5"
  }, auctionText, " "), /*#__PURE__*/_react["default"].createElement("span", null, "11d"), /*#__PURE__*/_react["default"].createElement("span", null, "11h"), /*#__PURE__*/_react["default"].createElement("span", null, "11s")), /*#__PURE__*/_react["default"].createElement("h1", {
    className: "auction__title text-lv5 text-center"
  }, title), /*#__PURE__*/_react["default"].createElement("h3", {
    className: "auction__subTitle text-lv7 text-center"
  }, subTitle))));
}

var _default = AuctionHeader;
exports["default"] = _default;