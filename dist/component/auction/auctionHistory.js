"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _lodash = require("lodash");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  .auction__inner {\n    background-color: ", ";\n  }\n"])), function (props) {
  return props.BgColor;
});

function AuctionHistory(_ref) {
  var bidHistoryData = _ref.bidHistoryData,
      BgColor = _ref.BgColor;
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid__history"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bid_history-inner"
  }, /*#__PURE__*/_react["default"].createElement("table", null, /*#__PURE__*/_react["default"].createElement("tbody", null, !(0, _lodash.isEmpty)(bidHistoryData) && /*#__PURE__*/_react["default"].createElement("tr", null, /*#__PURE__*/_react["default"].createElement("td", {
    className: "bid__history-name text-lv8 text-left"
  }, "user"), /*#__PURE__*/_react["default"].createElement("td", {
    className: "bid__history-time text-lv8 text-left"
  }, "price"), /*#__PURE__*/_react["default"].createElement("td", {
    className: "bid__history-price text-lv8 text-right"
  }, "time")), bidHistoryData.map(function (data) {
    return /*#__PURE__*/_react["default"].createElement("tr", null, /*#__PURE__*/_react["default"].createElement("td", {
      className: "bid__history-name text-lv8 text-left"
    }, (0, _lodash.get)(data, "id", "")), /*#__PURE__*/_react["default"].createElement("td", {
      className: "bid__history-price text-lv8 text-left"
    }, (0, _lodash.get)(data, "price", "")), /*#__PURE__*/_react["default"].createElement("td", {
      className: "bid__history-time text-lv8 text-right"
    }, (0, _lodash.get)(data, "time", "")));
  }))))));
}

var _default = AuctionHistory;
exports["default"] = _default;