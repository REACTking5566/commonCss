"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _withCircle = require("../withCircle");

var _withBorder = require("../withBorder");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyleWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  /* .big-card__content {\n    position: relative;\n    .content-footer {\n      position: absolute;\n      bottom: 0%;\n    }\n    .card__fav-icon {\n      position: absolute;\n      right: 0;\n    }\n  } */\n  .basicAlert {\n    position: fixed;\n    right: 20px;\n    bottom: 20px;\n    width: 350px;\n    z-index: 5;\n    .basic-alert-btn {\n    }\n  }\n"])));

function BasicAlert() {
  return /*#__PURE__*/_react["default"].createElement(StyleWrapper, null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "basicAlert"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    bgColor: "light",
    boxShadow: "0 0.55rem 1.5rem rgba(34,34,34,0)",
    padding: "15px"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "register-slide-in d-flex justify-between"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "basic-alert-title text-lv6"
  }, "\u9084\u672A\u8A3B\u518A"), /*#__PURE__*/_react["default"].createElement(_withCircle.Circle, {
    circleSize: "base",
    borderColor: "gray"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "fas fa-times-circle"
  }))), /*#__PURE__*/_react["default"].createElement("p", null, "\u7576\u60A8\u8A3B\u518A\u60A8\u7684Catawiki\u514D\u8CBB\u5E33\u6236\uFF0C\u60A8\u5C07\u80FD\u5920\u5728\u6211\u5011\u6BCF\u9031\u905450000\u4EF6\u7279\u5225\u7269\u54C1\u4E2D\u7684\u62CD\u8CE3\u4E0A\u7AF6\u6295\u3002"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "basic-alert-btn col-md-6"
  }, /*#__PURE__*/_react["default"].createElement(_withBorder.WithBorder, {
    bgColor: "warning",
    borderRadius: "26",
    padding: "10px 0",
    textAlign: "center"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: ""
  }, /*#__PURE__*/_react["default"].createElement("span", null, "\u5EFA\u7ACB\u65B0\u5E33\u6236")))))));
}

var _default = BasicAlert;
exports["default"] = _default;