"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Tag = exports.Circle = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CircleWrapper = _styledComponents["default"].button(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: ", ";\n  height: ", ";\n  border-radius: ", ";\n  border: 1px solid ", ";\n"])), function (_ref) {
  var circleSize = _ref.circleSize,
      size = _ref.theme.size;
  return size[circleSize];
}, function (_ref2) {
  var circleSize = _ref2.circleSize,
      size = _ref2.theme.size;
  return size[circleSize];
}, function (_ref3) {
  var circleSize = _ref3.circleSize,
      size = _ref3.theme.size;
  return size[circleSize];
}, function (props) {
  return props.borderColor;
});

var Circle = function Circle(_ref4) {
  var borderColor = _ref4.borderColor,
      circleSize = _ref4.circleSize,
      children = _ref4.children;
  return /*#__PURE__*/_react["default"].createElement(CircleWrapper, {
    borderColor: borderColor,
    circleSize: circleSize
  }, children);
};

exports.Circle = Circle;

var StyleTag = _styledComponents["default"].div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  position: relative;\n"])));

var Tag = function Tag(_ref5) {
  var children = _ref5.children;
  return /*#__PURE__*/_react["default"].createElement(StyleTag, null, children);
};

exports.Tag = Tag;