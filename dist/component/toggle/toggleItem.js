"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _toggleWrapper = _interopRequireDefault(require("./toggleWrapper"));

var _reactstrap = require("reactstrap");

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ToggleItem = function ToggleItem(_ref) {
  var toggleTitle = _ref.toggleTitle,
      children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement(_toggleWrapper["default"], {
    toggle: function toggle(show, hide, showModal) {
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "filter__dropDown",
        onClick: function onClick() {
          if (showModal) {
            hide();
          } else {
            show();
          }
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "filter__item d-flex justify-between justify-center"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "filter__item-name text-lv7"
      }, toggleTitle), /*#__PURE__*/_react["default"].createElement("span", {
        className: "filter__item-icon"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "".concat((0, _classnames["default"])("fas", {
          "fa-caret-right": !showModal
        }, {
          "fa-caret-down": showModal
        }))
      }))));
    },
    content: function content(showModal) {
      return /*#__PURE__*/_react["default"].createElement(_reactstrap.Collapse, {
        isOpen: showModal
      }, children);
    }
  });
};

var _default = ToggleItem;
exports["default"] = _default;