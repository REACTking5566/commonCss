"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Tag = exports.CardTag = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CardWrapper = _styledComponents["default"].div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  .card__icon {\n    position: absolute;\n    left: ", "px;\n    top: ", "px;\n    width: ", "%;\n    z-index: 2;\n    color: ", ";\n    background: ", ";\n    span {\n      display: inline-block;\n      padding: ", ";\n    } \n    &:after {\n      content: \"\";\n      display: ", ";\n      position: absolute;\n      left: 0;\n      top: 100%;\n      width: 0;\n      height: 0;\n      border-left: 15px solid transparent;\n      border-top: 15px solid #424257;\n    }\n  }\n"])), function (_ref) {
  var left = _ref.left;
  return left;
}, function (_ref2) {
  var top = _ref2.top;
  return top;
}, function (_ref3) {
  var width = _ref3.width;
  return width;
}, function (_ref4) {
  var tagTextColor = _ref4.tagTextColor;
  return tagTextColor || "#fff";
}, function (_ref5) {
  var tagBgColor = _ref5.tagBgColor;
  return tagBgColor || "#424257";
}, function (_ref6) {
  var padding = _ref6.padding;
  return padding;
}, function (_ref7) {
  var tagStyle = _ref7.tagStyle;
  return tagStyle === "triangle" ? "block" : "none";
});

var CardTag = function CardTag(_ref8) {
  var left = _ref8.left,
      top = _ref8.top,
      children = _ref8.children,
      width = _ref8.width,
      text = _ref8.text,
      tagTextColor = _ref8.tagTextColor,
      tagBgColor = _ref8.tagBgColor,
      tagStyle = _ref8.tagStyle,
      padding = _ref8.padding;
  return /*#__PURE__*/_react["default"].createElement(CardWrapper, {
    left: left,
    top: top,
    width: width,
    tagTextColor: tagTextColor,
    tagBgColor: tagBgColor,
    tagStyle: tagStyle,
    padding: padding
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "card__icon text-center"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-lv-5"
  }, text)), children);
};

exports.CardTag = CardTag;

var StyleTag = _styledComponents["default"].div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  position: relative;\n"])));

var Tag = function Tag(_ref9) {
  var children = _ref9.children;
  return /*#__PURE__*/_react["default"].createElement(StyleTag, null, children);
};

exports.Tag = Tag;