"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.AXIOS_METHOD_TYPE = void 0;

var _axios2 = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AXIOS_METHOD_TYPE = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH'
};
/** 方法说明
 * @method request
 * @param api 请求相对地址
 * @param method 请求方法，默认get
 * @param params 请求参数 默认为空
 * @param config 请求配置 默认为空
 * @return function 返回axios实例
*/

exports.AXIOS_METHOD_TYPE = AXIOS_METHOD_TYPE;

var request = function request(api) {
  var _axios;

  var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : AXIOS_METHOD_TYPE.GET;
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var config = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  method = method.toLocaleUpperCase();
  console.log(params); // get请求放在params中，其他请求放在body

  var data = method === 'GET' ? 'params' : 'data'; // 这部分也可以放到defaults中去设置

  var headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  };

  if (config.headers) {
    headers = _objectSpread(_objectSpread({}, headers), config.headers);
  }

  return (0, _axios2["default"])((_axios = {
    url: api,
    method: method
  }, _defineProperty(_axios, data, params), _defineProperty(_axios, "headers", headers), _axios));
};

var _default = request;
exports["default"] = _default;