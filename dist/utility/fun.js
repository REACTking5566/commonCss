"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timeCountDown = exports.AryItem = void 0;

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AryItem = function AryItem(size) {
  return function (Ele) {
    return Array(size).fill({}).map(function () {
      return Ele;
    });
  };
};

exports.AryItem = AryItem;

var timeCountDown = function timeCountDown(seconds, dom) {
  var timeElement, eventTime, currentTime, duration, interval, intervalId;
  interval = 1000; // 1 second
  // get time element
  // calculate difference between two times

  eventTime = _momentTimezone["default"].tz('2020-06-15T09:00:00', 'Asia/Taipei'); // based on time set in user's computer time / OS

  currentTime = _momentTimezone["default"].tz(); // get duration between two times

  duration = _momentTimezone["default"].duration(eventTime.add(seconds, 'seconds').diff(currentTime));
  timeElement = dom; // loop to countdown every 1 second

  var intervalInstance = function intervalInstance() {
    // get updated duration
    duration = _momentTimezone["default"].duration(duration - interval, 'milliseconds'); // if duration is >= 0

    if (duration.asSeconds() <= 0) {
      clearInterval(intervalId); // hide the countdown element

      timeElement.classList.add('hidden');
    } else {
      // otherwise, show the updated countdown
      if (timeElement) {
        timeElement.innerText = duration.days() + ' days ' + duration.hours() + ' hours ' + duration.minutes() + ' minutes ' + duration.seconds() + ' seconds';
      }
    }
  };

  return intervalInstance(); // clear && clearInterval(intervalInstance);
};

exports.timeCountDown = timeCountDown;