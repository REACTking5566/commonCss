"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var color = {
  primary: '#1976D2',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107',
  card: '#f8f1db',
  transparent: 'transparent',
  light: '#fcfcfc',
  dark: '#343a40'
};
var base = 2;
var size = {
  xl: "".concat(1.875 * base, "rem"),
  lg: "".concat(1.375 * base, "rem"),
  md: "".concat(1.25 * base, "rem"),
  base: "".concat(1 * base, "rem"),
  sm: "".concat(0.875 * base, "rem")
};
var _default = {
  size: size,
  color: color
};
exports["default"] = _default;