import React from "react";
import { action } from '@storybook/addon-actions';
import { addons, types } from '@storybook/addons';
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import BasicCard from "../src/component/card/basicCard";
import '../src/css/common.scss';

export default {
    title: 'BasicCard',
    decorators: [withKnobs],
    excludeStories: /.*Data$/,
};

export const BasicCardItem = () => {
    return (
        <div style={{ maxWidth: '500px' }}>
            <BasicCard
                price={1000}
                describle="werewrewsdfsdfsdfsdfewrewrewsdfdsfs"
                imgUrl="https://assets.catawiki.nl/assets/2019/10/14/8/c/2/thumb2_8c2b215d-0397-426c-ab99-03b6ea8a64a3.jpg"
            />
        </div>
    )
};