import React from "react";
import { action } from '@storybook/addon-actions';
import { addons, types } from '@storybook/addons';
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import Footer from '../src/component/test';


export default {
    title: 'test',
    // component: Test,
    decorators: [withKnobs],
    excludeStories: /.*Data$/,
};

export const storyText = () => {
    return <Footer text={text('tt', '5566')} />;
};